# GlobaliD Connect - Web Client Launcher

_GlobaliD's web client_ provides a javascript implementation of GlobaliD connect, allowing users to create and manage their identity through a secure web portal. The source code is available on GitLab at [globalid/opensource/web-client-launcher](https://gitlab.com/globalid/opensource/web-client-launcher).

With GlobaliD, users are enabled to securely attest and share personally identifiable information.

Third party applications can, with the users consent, be granted limited access to the users identity and its capabilities within the GlobaliD ecosystem.

The Web Client Launcher is [an npm package](https://www.npmjs.com/package/globalid-web-client-launcher). It can be installed with `npm install globalid-web-client-launcher`

To be able to use this, you will need to create an application on the GlobaliD Developer panel: ([https://developer.global.id](https://developer.global.id))

## Example

To create a login with restrictions flow, you must specify required verifications (currently via an _acrc_id_). You can view the required verifications here: [api.globalid.net/demo/acrc/36daae9e-4dd6-459d-bae8-051206f59b41](https://api.globalid.net/demo/acrc/36daae9e-4dd6-459d-bae8-051206f59b41). To create your own app's required verifications, contact GlobaliD.


```javascript
import { gid } from 'globalid-web-client-launcher';

const config = {
  client_id: '2ad2164f-8faf-4322-aa2e-3c88807ff000', // From the developer portal: https://developer.global.id
  acrc_uuid: '36daae9e-4dd6-459d-bae8-051206f59b41', // Contact GlobaliD to create
  default_oauth_options: {
    grant_type: 'code'
  },
  redirect_url: 'https://example.com',
  scopes: [ 'public' ]
};

gid.init(config);

gid.onComplete(onCompleteHandler);
gid.onError(onErrorHandler);
```

_**Note:** At this moment only `code` (Authorization code) grant type is supported by web client._

### Configuration
Configuration

* acrc_uuid: The ACRC ID of attestation requirements
* client_id: Your globaliD Connect client id
* acrc_id: Attestation consent request id
* default_oauth_options: OAuth2 options object
* * grant_type: `code`
* redirect_url: A whitelisted url for your globaliD Connect client
* scopes: Access scopes the user will grant permission for

### Methods

`.init(config)`
> Saves the configuration parameters

`.login()`
> Start the web client login flow

`.signup(FlowTypes.TWILIO)`
> Start the web client signup flow, using the Twilio attestation to verify to confirm the user

`.changePassword()`
> Start the change password flow, for already authenticated users

`.onComplete(onCompleteHandler)`
> Handler function is passed data for retrieving the access token when the user successfully completes the web client flow.

`.onError(onErrorHandler)`
> Handler function is passed error data if an unrecoverable error occurs or the user rejects the authentication request.

```javascript
const onCompleteHandler = async function (webClientResponse) {
  const response = await axios.request({
    url: '/auth/token',
    baseURL: 'https://api.global.id',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: {
     client_id: <client_id>,
     client_secret: <client_secret>,
     redirect_uri: <redirect_uri>,
     code: webClientResponse.data.,
     grant_type: 'authorization_code'
    }
  })
    
  const { access_token, refresh_token } = response;
  ...
}
```

---

## Development

- View scripts for auto-linting, tests, running in dev, etc in [package.json](package.json)
- View [.gitlab-ci.yml](.gitlab-ci.yml) for how the npm package is published
