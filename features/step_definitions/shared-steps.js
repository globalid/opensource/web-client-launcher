const chai = require('chai');
const expect = chai.expect;
const pages = require('./support/pages');

const { Given: given, Then: then, When: when } = require('cucumber');
const { By: by } = require('selenium-webdriver');

given('I am on the {string} page', async function(page) {
  return await this.driver.get(pages.getUrl(page));
});

then('I should see the {string} page', async function(page) {
  const title = await this.driver.getTitle();
  console.log(page, title, pages);
  const titleEl = await this.driver.findElement(by.tagName('title'));
  console.log(titleEl);
  return expect(title).to.equal(pages.getTitle(page));
});

then('I should see the \'login\' button', async function() {
  const loginButton = this.driver.findElement(by.id('login'));

  return expect(loginButton).to.not.equal(undefined);
});

then('I should click the \'login\' button', async function() {
  const loginButton = this.driver.findElement(by.id('login'));

  const initHandles = await this.driver.getAllWindowHandles();

  expect(initHandles.length).to.equal(1);

  await loginButton.click();
});

then('I should see the \'popup\' window', async function() {
  const afterClickHandles = await this.driver.getAllWindowHandles();

  expect(afterClickHandles.length).to.equal(2);
});

then('I should click the {string} button in the {string} window', async function(buttonName, windowName) {
  const handles = await this.driver.getAllWindowHandles();
  popupWindow = handles[1];

  await this.driver.switchTo().window(popupWindow);

  const closeButton = this.driver.findElement(by.id(buttonName));
  await closeButton.click();
});

then('I should see the {string} window close', async function(windowName) {
  const newHandles = await this.driver.getAllWindowHandles();

  expect(newHandles.length).to.equal(1);
});
