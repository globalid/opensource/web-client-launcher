const chai = require('chai');
const NetworkInterceptor = require('./support/network-interceptor');

const expect = chai.expect;

const { Then: then, When: when} = require('cucumber');

const builder = require('./support/builder');

// General methods
when('I wait for {int} seconds', function (string) {
  return this.driver.sleep(Number(string) * 1000);
});

