/**
 * This file is used to remote debug Chrome
 */
const cdp = require('chrome-remote-interface');
const btoa = require('btoa');

let protocol;
let Network;

const networkIsReady = async () => {
  // TODO - remote debugging port - envify this
  protocol = await cdp({ port: 9002 });
  Network = protocol.Network;

  return await Promise.all([Network.enable()]);
};

const registerInterceptionRequest = async (params) => {

  console.log('l0gGing request interception');

  await networkIsReady();

  await Network.setRequestInterception({
    patterns: [{
      urlPattern: params.urlPattern,
      interceptionStage: 'HeadersReceived'
    }]
  });

  Network.requestIntercepted(async ({ interceptionId, request }) => {
    
    console.log(`Intercepted ${request.url} {interception id: ${interceptionId}}`);

    const newBody = JSON.stringify(params.responseObject);

    const newHeaders = [
      'HTTP/1.1 200 OK',
      'Date: ' + (new Date()).toUTCString(),
      'Connection: closed',
      'Content-Length: ' + newBody.length,
      'Content-Type: application/json; charset=utf-8;',
      'Access-Control-Allow-Credentials: true',
      'Access-Control-Allow-Origin: http://localhost:9000'
    ];
    // TODO - switch the localhost:9000 above with an env variable
    // pointing to the host which cucumber launches.

    Network.continueInterceptedRequest({
      interceptionId,
      rawResponse: btoa(newHeaders.join('\r\n') + '\r\n\r\n' + newBody)
    });
  });

  return;
};

const getProtocol = () => protocol;

module.exports = {
  getProtocol,
  registerInterceptionRequest,
};
