const helpers = require('./test-helpers');

const urls = {
  index: helpers.getHost() + '/index.html',
  demo: helpers.getHost() + '/index.html',
};

const titles = {
  index: 'globaliD Connect',
  demo: 'globaliD Connect Popup Demo'
};

module.exports = {
  getTitle: (page) => titles[page],
  getUrl: (page) => urls[page],
};
