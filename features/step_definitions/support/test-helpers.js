const dotenv = require('dotenv');

const webdriver = require('selenium-webdriver');
const { setDefaultTimeout } = require('cucumber');

dotenv.config();

const HOST = process.env.HOST || 'http://0.0.0.0:3000/';
const BROWSER = process.env.BROWSER || 'chrome';
const BROWSER_WIDTH = parseInt(process.env.BROWSER_WIDTH, 10) || 1280;
const BROWSER_HEIGHT = parseInt(process.env.BROWSER_HEIGHT, 10) || 800;
const SCRIPT_TIMEOUT = parseInt(process.env.SCRIPT_TIMEOUT, 10) || 5000;
const HEADLESS = !!process.env.HEADLESS;

webdriver.promise.USE_PROMISE_MANAGER = false;

setDefaultTimeout(SCRIPT_TIMEOUT);

const urls = {
  home: HOST,
  demo: HOST,
};

const headlessSettings = {
  chrome: (options) => {
    options.chromeOptions = {
      args: ['--headless', '--no-sandbox'],
    };

    return options;
  },
};

module.exports = {
  destroyDriver: function() {
    this.driver.quit();
  },

  driver: null,

  getDriver: async function() {

    let capabilities = {
      acceptInsecureCerts: true,
      browserName: BROWSER,
      loglevel: 'verbose',
    };

    if (HEADLESS) {
      capabilities = headlessSettings[BROWSER](capabilities);
    }

    const driver = await new webdriver.Builder()
        .withCapabilities(capabilities)
        .build();

    driver.manage().window().setRect({
      height: BROWSER_HEIGHT,
      width: BROWSER_WIDTH,
    });

    return driver;
  },

  getHost: () => HOST,

  getTimeout: function() {
    return SCRIPT_TIMEOUT;
  },

  getURL: (key) => urls[key],
};
