const { By: by } = require('selenium-webdriver');

const getElementsByClassName = async (ctx, className) => await ctx.driver.findElements(by.xpath(`//*[contains(@class, '${className}')]`));
const getChildByTagName = async (el, tagName) => await el.findElement(by.xpath(`//${tagName}`));

const getChildByDataAttribute = async (el, dataAttribute, value) => {
  return await el.findElement(by.css(`[${dataAttribute}=${value}]`));
};

const getChildByClassName = async (el, classToLookup) => {
  return await el.findElement(by.css(`${classToLookup}`));
};

const builder = (ctx) => ({

 // Add helper methods here

  elementByCssExists: async(cssValue) => {
    const element = await ctx.driver.findElements(by.css(cssValue));

    if(element.length === 0) {
      return false;
    } else {
      return true;
    }
  },

  pageHasScanAgainAnchor: async() => {
    const scanAgainAnchor = await ctx.driver.findElements(by.xpath("//a[contains(.,'SCAN AGAIN')]"));

    if(scanAgainAnchor.length === 0) {
      return false;
    } else {
      return true;
    }
  },

  pageHasSupportAnchor: async() => {
    // looks out for the 'contact support' button
    const supportAnchor = await ctx.driver.findElements(by.xpath("//a[contains(@href,'mailto:support@global.id')]"));

    if(supportAnchor.length === 0) {
      return false;
    } else {
      return true;
    }
  },

  getCurrentUrl: async() => {
    return await ctx.driver.getCurrentUrl().then((url) => {
      return url;
    });
  }
});

module.exports = builder;
