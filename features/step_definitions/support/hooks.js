const { After: after, Before: before, Status: status } = require('cucumber');
const helpers = require('./test-helpers');

before(async function() {
  this.driver = await helpers.getDriver();
});

after(function(testCase) {

  const world = this;

  if (testCase.result.status === status.FAILED) {
    return this.driver.takeScreenshot().then(function(screenShot) {
      // screenShot is a base-64 encoded PNG
      world.attach(screenShot, 'image/png');
    });
  }

  this.driver.quit();
});
