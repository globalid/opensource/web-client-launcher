Feature: Popup Window
  As a user
  I want to access the demo page
  So that I can launch the popup window

  @demo-page
  Scenario: Log in
    When I wait for 10 seconds
    Given I am on the 'demo' page
    Then I should see the 'demo' page
    Then I should see the 'login' button
    Then I should click the 'login' button
    Then I should see the 'popup' window
    Then I should click the 'close' button in the 'popup' window
    Then I should see the 'popup' window close
