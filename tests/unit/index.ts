// tslint:disable:no-duplicate-string no-identical-functions

// @ts-ignore

/**
 * @file unit tests for gid piblic sdk entry point
 */

import { Transceiver } from 'globalid-window-messaging/dist';
import {
  CallbackPayload,
  FlowTypes,
  gid,
  GidConfigInterface,
  GrantTypes,
} from '../../src';

const defaultConfigValues: GidConfigInterface = {
  agencies_url: 'https://agencies-api.global.id',
  api_base_url: 'https://api.global.id',
  client_id: 'test',
  default_oauth_options: { grant_type: GrantTypes.code },
  gid_connect_base_url: 'https://auth.global.id/',
  gid_connect_url: 'https://auth.global.id/web-client/popup.html',
  redirect_url: 'http://localhost',
};

describe('gid public sdk interface', () => {
  let transceiverInitSpy: jasmine.Spy;
  let windowOpenSpy: jasmine.Spy;
  let documentCreateElementSpy: jasmine.Spy;
  let documentGetElementSpy: jasmine.Spy;
  let sendMessageSpy: jasmine.Spy;
  let setAttributeSpy: jasmine.Spy;
  let listener: Function;
  let config: GidConfigInterface;
  let mockPopup: object;
  let mockCreateElement: object;
  let mockGetElement: object;

  let dummyElement: HTMLElement;
  beforeEach(() => {
    config = {
      ...defaultConfigValues,
    };
    dummyElement = document.createElement('div');
    dummyElement.appendChild(document.createElement('div'));
    mockPopup = {
      postMessage: (): object => ({}),
    };
    setAttributeSpy = jasmine.createSpy();
    mockCreateElement = {
      contentWindow: mockPopup,
      setAttribute: setAttributeSpy,
      src: jasmine.createSpy(),
      style: {},
    };
    mockGetElement = {
      appendChild: (): object => ({}),
    };

    transceiverInitSpy = spyOn(Transceiver.prototype, 'init').and.callThrough();
    windowOpenSpy = spyOn(window, 'open').and.returnValue(mockPopup);
    documentCreateElementSpy = spyOn(document, 'createElement').and.returnValue(
      mockCreateElement,
    );
    documentGetElementSpy = spyOn(document, 'getElementById').and.returnValue(
      mockGetElement,
    );
    sendMessageSpy = spyOn(
      Transceiver.prototype,
      'sendMessage',
    ).and.callThrough();

    transceiverInitSpy.calls.reset();
    windowOpenSpy.calls.reset();
    documentCreateElementSpy.calls.reset();
    documentGetElementSpy.calls.reset();
    sendMessageSpy.calls.reset();
  });

  afterEach(() => {
    gid.clear();
  });

  describe('init', () => {
    it('should define an init method', () => {
      expect(typeof gid.init).toEqual('function');
    });

    it('should add a message event listener', () => {
      gid.init(config);

      expect(typeof transceiverInitSpy.calls.argsFor(0)[0]).toEqual('function');
    });

    it('should set the right config', () => {
      const partialConfig: GidConfigInterface = {
        agencies_url: 'undefined',
        api_base_url: 'undefined',
        client_id: 'client_id',
        default_oauth_options: config.default_oauth_options,
        gid_connect_base_url: undefined,
        gid_connect_url: 'foo',
        redirect_url: 'undefined',
      }

      gid.init(partialConfig);
      gid.login();

      listener = transceiverInitSpy.calls.argsFor(0)[0];
      listener({
        event: 'web-client-ready',
      });

      expect(sendMessageSpy).toHaveBeenCalledWith(
        {
          app_uuid: [],
          config: {
            ...partialConfig,
            agencies_url: 'https://agencies-api.global.id',
            api_base_url: 'https://api.global.id',
            gid_connect_base_url: 'https://auth.global.id/',
            gid_connect_url: 'foo',
          },
          event: 'web-client-widget-request',
          flow: 'LOGIN',
        },
        'web-client',
      );
    });
  });

  // Also covers tests for renderLogin as the only implementation
  // difference is covered at the very end of this test file
  describe('render', () => {
    beforeEach(() => {
      config = {
        ...defaultConfigValues,
      };
    });

    it('should create a popup window with default location', () => {
      gid.init(config);
      gid.login();

      expect(windowOpenSpy.calls.argsFor(0)[0]).toEqual(
        'https://auth.global.id/web-client/popup.html?client_id=test&redirect_uri=http://localhost&response_type=code',
      );
      expect(windowOpenSpy.calls.argsFor(0)[1]).toEqual('web-client');

      // tslint:disable-next-line:no-unused-expression
      expect(windowOpenSpy.calls.argsFor(0)[2].length > 0).toBe(true);

      const valuesToSearchFor: string[] = [
        'height',
        'resizable=no',
        'scrollbars=no',
        'top',
        'width',
      ];

      valuesToSearchFor.forEach((searchVal: string) => {
        expect(windowOpenSpy.calls.argsFor(0)[2].indexOf(searchVal)).not.toBe(
          -1,
        );
      });
    });
    it('should create a IFrame with default location', () => {
      gid.init({
        iframe_container_id: 'iframeId',
        ...config,
      });
      gid.login();

      expect(documentCreateElementSpy.calls.argsFor(0)[0]).toEqual('div');
      expect(documentCreateElementSpy.calls.argsFor(1)[0]).toEqual('iframe');
      expect(setAttributeSpy.calls.argsFor(0)[1]).toEqual('web-client');
      expect(documentGetElementSpy.calls.argsFor(0)[0]).toEqual('iframeId');
    });
    it('should create a IFrame and override the style with window_features', () => {
      gid.init({
        iframe_container_id: 'iframeId',
        ...config,
        iframe_style: {
          border: 'none',
          height: '200',
        },
      });
      gid.login();

      expect((<{ style: object }> mockCreateElement).style).toEqual(
        jasmine.objectContaining({
          border: 'none',
          height: '200',
        }),
      );
    });
    it('should propagate error when trying to create a IFrame and parent container is missing', () => {
      gid.init({
        iframe_container_id: 'iframeId',
        ...config,
      });
      documentGetElementSpy.and.returnValue(null);
      try {
        gid.login();
      } catch (err) {
        expect(err.message).toBe('IFrame container is missing.');

        return;
      }
      throw new Error('Error was not thrown when container is missing');
    });
    it('should allow override of url', () => {
      config.gid_connect_url = 'https://foo.com/bar';

      gid.init(config);
      gid.login();

      expect(windowOpenSpy.calls.argsFor(0)[0]).toEqual(
        'https://foo.com/bar?client_id=test&redirect_uri=http://localhost&response_type=code',
      );
    });

    it('should allow override of window name', () => {
      config.window_name = 'foo';

      gid.init(config);
      gid.login();

      expect(windowOpenSpy.calls.argsFor(0)[1]).toEqual('foo');
    });

    it('should allow override of window features', () => {
      config.window_features = {
        scrollbars: true,
      };

      gid.init(config);
      gid.login();

      expect(windowOpenSpy.calls.argsFor(0)[0]).toEqual(
        'https://auth.global.id/web-client/popup.html?client_id=test&redirect_uri=http://localhost&response_type=code',
      );
      expect(windowOpenSpy.calls.argsFor(0)[1]).toEqual('web-client');

      // tslint:disable-next-line:no-unused-expression
      expect(windowOpenSpy.calls.argsFor(0)[2].length > 0).toBe(true);

      const valuesToSearchFor: string[] = [
        'height',
        'resizable=no',
        'scrollbars=yes',
        'top',
        'width',
      ];

      valuesToSearchFor.forEach((searchVal: string) => {
        expect(windowOpenSpy.calls.argsFor(0)[2].indexOf(searchVal)).not.toBe(
          -1,
        );
      });
    });
  });

  describe('message event listener', () => {
    it('should do nothing if popup has not been instantiated', () => {
      gid.init(config);

      listener = transceiverInitSpy.calls.argsFor(0)[0];
      listener({
        event: 'web-client-ready',
      });

      expect(sendMessageSpy).not.toHaveBeenCalled();
    });

    it('should do nothing if popup has been instantiated but message is not recognised', () => {
      gid.init(config);

      listener = transceiverInitSpy.calls.argsFor(0)[0];
      listener({
        event: 'foo',
      });

      expect(sendMessageSpy).not.toHaveBeenCalled();
    });

    describe('when the message is recognised', () => {
      it('should call window postMessage for the login flow when login is called', () => {
        gid.init(config);
        gid.login();

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: [],
            config: { ...defaultConfigValues },
            event: 'web-client-widget-request',
            flow: 'LOGIN',
          },
          'web-client',
        );
      });

      it('should call window postMessage for the Signup Flow when SIGN_UP is explicitly called', () => {
        gid.init(config);
        gid.signup(FlowTypes.TWILIO);

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: [],
            config: { ...defaultConfigValues },
            event: 'web-client-widget-request',
            flow: ['SIGN_UP', 'TWILIO'],
          },
          'web-client',
        );
      });

      it('should pass the config to the popup window', () => {
        gid.init({
          ...config,
          version: 'v10.0.0',
        });
        gid.signup(FlowTypes.TWILIO);

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: [],
            config: { ...config, version: 'v10.0.0' },
            event: 'web-client-widget-request',
            flow: ['SIGN_UP', 'TWILIO'],
          },
          'web-client',
        );
      });

      it('should call window postMessage for the attestation flow', () => {
        gid.init(config);
        gid.attest(['ea215db9-ce79-4bb3-b9eb-3d90f5455d52']);

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: ['ea215db9-ce79-4bb3-b9eb-3d90f5455d52'],
            config: { ...defaultConfigValues },
            event: 'web-client-widget-request',
            flow: 'ATTESTATION',
          },
          'web-client',
        );
      });

      it('should call window postMessage for given attestations', () => {
        gid.init(config);
        gid.attest(['ea215db9-ce79-4bb3-b9eb-3d90f5455d52']);

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: ['ea215db9-ce79-4bb3-b9eb-3d90f5455d52'],
            config: { ...defaultConfigValues },
            event: 'web-client-widget-request',
            flow: 'ATTESTATION',
          },
          'web-client',
        );
      });

      it('should call window postMessage for the given sandbox attestation flow', () => {
        gid.init(config);
        gid.attestSandbox(['233546ab-5c0b-4c7e-8f38-e899545a96a2']);

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: ['233546ab-5c0b-4c7e-8f38-e899545a96a2'],
            config: { ...defaultConfigValues },
            event: 'web-client-widget-request',
            flow: 'SANDBOX_ATTESTATION',
          },
          'web-client',
        );
      });

      it('should call window postMessage for upgrade to globalid flow', () => {
        gid.init(config);
        gid.upgradeToGlobaliD();

        listener = transceiverInitSpy.calls.argsFor(0)[0];
        listener({
          event: 'web-client-ready',
        });

        expect(sendMessageSpy).toHaveBeenCalledWith(
          {
            app_uuid: [],
            config: { ...defaultConfigValues },
            event: 'web-client-widget-request',
            flow: 'UPGRADE_TO_GLOBALID',
          },
          'web-client',
        );
      });

      it('should clear IFrame container DIV', () => {
        gid.init({
          iframe_container_id: 'iframeId',
          ...config,
        });
        dummyElement.setAttribute('id', 'iframeId');
        expect(dummyElement.childNodes.length).toEqual(1);
        documentGetElementSpy.and.returnValue(dummyElement);
        gid.clearIFrameContainer();
        expect(dummyElement.childNodes.length).toEqual(0);
      });

      describe('signup duck-typing', () => {
        it('should accept FlowTypes', () => {
          gid.init(config);
          gid.signup(FlowTypes.TWILIO);

          listener = transceiverInitSpy.calls.argsFor(0)[0];
          listener({
            event: 'web-client-ready',
          });

          expect(sendMessageSpy).toHaveBeenCalledWith(
            {
              app_uuid: [],
              config: { ...defaultConfigValues },
              event: 'web-client-widget-request',
              flow: ['SIGN_UP', 'TWILIO'],
            },
            'web-client',
          );
        });

        it('should accept FlowTypes', () => {
          gid.init(config);
          gid.signup([FlowTypes.TWILIO]);

          listener = transceiverInitSpy.calls.argsFor(0)[0];
          listener({
            event: 'web-client-ready',
          });

          expect(sendMessageSpy).toHaveBeenCalledWith(
            {
              app_uuid: [],
              config: { ...defaultConfigValues },
              event: 'web-client-widget-request',
              flow: ['SIGN_UP', 'TWILIO'],
            },
            'web-client',
          );
        });
      });

      describe('onComplete', () => {
        it('should call the registered data handler function', () => {
          const fn: (payload: CallbackPayload) => void = jasmine.createSpy(
            'completeListener',
          );
          gid.init(config);
          gid.login();
          gid.onComplete(fn);

          listener = transceiverInitSpy.calls.argsFor(0)[0];
          listener({
            data: { a: 'payload' },
            event: 'web-client-complete',
          });

          expect(fn).toHaveBeenCalledWith({ a: 'payload' });
        });
      });

      describe('onError', () => {
        it('should call the registered error handler function', () => {
          const fn: (payload: CallbackPayload) => void = jasmine.createSpy(
            'errorListener',
          );
          gid.init(config);
          gid.login();
          gid.onError(fn);

          listener = transceiverInitSpy.calls.argsFor(0)[0];
          listener({
            error: { a: 'payload' },
            event: 'web-client-error',
          });

          expect(fn).toHaveBeenCalledWith({ a: 'payload' });
        });
      });
    });
  });
});
