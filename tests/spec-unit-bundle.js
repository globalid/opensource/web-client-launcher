Error.stackTraceLimit = Infinity;

const testContext = require.context('./unit', true, /\.ts/);

const requireAll = (requireContext) => {
  return requireContext.keys().map(requireContext);
};

requireAll(testContext);
