// tslint:disable:no-suspicious-comment
/**
 * @file globaliD public sdk
 */
import { Transceiver } from 'globalid-window-messaging/dist';

import { windowFeatures } from './window_features';

const transceiver: Transceiver = new Transceiver();

const DEFAULT_API_BASE_URL: string = 'https://api.global.id';

const DEFAULT_AGENCIES_BASE_URL: string = 'https://agencies-api.global.id';

const DEFAULT_WEB_CLIENT_URL: string =
  'https://auth.global.id/web/popup.html';

const DEFAULT_GLOBALID_CONNECT_URL: string = 'https://auth.global.id/';
/**
 * Enum for auth grant-types.
 */
export enum GrantTypes {
  code = 'code',
  implicit = 'implicit',
}

/**
 * Enum for Flow types.
 */
export enum FlowTypes {
  MISSING_ATTESTATIONS = 'MISSING_ATTESTATIONS',
  ATTESTATION = 'ATTESTATION',
  SANDBOX_ATTESTATION = 'SANDBOX_ATTESTATION',
  ATTESTATION_APPROVAL = 'ATTESTATION_APPROVAL',
  SIGN_UP = 'SIGN_UP',
  LOGIN = 'LOGIN',
  TWILIO = 'TWILIO',
  FORGOT_PASSWORD = 'FORGOT_PASSWORD',
  CHANGE_PASSWORD = 'CHANGE_PASSWORD',
  UPGRADE_TO_GLOBALID = 'UPGRADE_TO_GLOBALID',
  CONSENT = 'CONSENT',
}

export interface OAuthOptions {
  grant_type: GrantTypes;
  state?: string;
}

export interface ConfigInterface {
  acrc_uuid?: string | null;
  agencies_url?: string;
  api_base_url?: string;
  client_id: string;
  redirect_url: string;
  nonce?: string;
  scopes?: string[];
  access_token?: string;
  default_oauth_options: OAuthOptions;
  // tslint:disable-next-line:no-any
  default_signup_options?: any;
  gid_connect_base_url?: string;
  version?: string;
  refresh_token?: string;
  pii_request_uuid?: string;
}

export interface PopupWinProps {
  url: string;
  winFeatures: object;
  winName: string;
  winSerializedFeatures: string;
}

export interface GidConfigInterface extends ConfigInterface {
  gid_connect_url?: string;
  window_name?: string;
  iframe_container_id?: string;
  iframe_style?: {
    [key: string]: string;
  };
  iframe_close_style?: {
    [key: string]: string;
  };
  window_features?: {
    [key: string]: string | boolean;
  };
}

export interface CallbackPayload {
  data: object;
  name: string;
}

export interface URLGidcParams {
  acrc_id?: string;
  client_id: string;
  redirect_uri: string;
  scope?: string;
  response_type: string;
  nonce?: string;
  state?: string;
}

export type Callback = (payload: CallbackPayload) => void;

let config: GidConfigInterface;
let popupName: string | undefined;

let completeListener: Callback = (callbackPayload: CallbackPayload): void =>
  undefined;
let errorListener: Callback = (callbackPayload: CallbackPayload): void =>
  undefined;

const gid_connect_url: string =
  process.env.GID_CONNECT_SIGNUP_URL !== undefined
    ? process.env.GID_CONNECT_SIGNUP_URL
    : DEFAULT_WEB_CLIENT_URL;

let currentlyLoadedFlow: FlowTypes | FlowTypes[] = [FlowTypes.SIGN_UP];
let app_uuid: string[] = [];

/**
 * handler for postMessage events
 *
 */
function listener (message: MessageEvent['data']): void {
  if (popupName === undefined) {
    return;
  }

  switch (message.event) {

    case 'web-client-ready':
      const payload: object = {
        app_uuid,
        config,
        event: 'web-client-widget-request',
        flow: currentlyLoadedFlow,
      };
      transceiver.sendMessage(payload, popupName);

      break;

    case 'web-client-complete':
      completeListener(<CallbackPayload> message.data);
      break;

    case 'web-client-error':
      errorListener(<CallbackPayload> message.error);
      break;

    default:
  }
}

const defaultConfigValues: Partial<GidConfigInterface> = {
  agencies_url: DEFAULT_AGENCIES_BASE_URL,
  api_base_url: DEFAULT_API_BASE_URL,
  gid_connect_base_url: DEFAULT_GLOBALID_CONNECT_URL,
  gid_connect_url: gid_connect_url,
};

/**
 * util for serializing window features config
 *
 */
function serialize (obj: object): string {
  return Object.keys(obj)
    .map(
      (key: string): string => {
        if (obj[key] === true) {
          return `${key}=yes`;
        }

        if (obj[key] === false) {
          return `${key}=no`;
        }

        return `${key}=${obj[key]}`;
      },
    )
    .join(',');
}

export namespace gid {
  /**
   * init passes ConfigInterface data to sdk
   * and listens for postMessage events from the popup
   */
  export function init (initConfig: GidConfigInterface): void {
    config = addDefaultsToConfig(defaultConfigValues, initConfig);
    transceiver.init(listener);
  }

  /**
   *  Adds default values to undefined config values
   */
  function addDefaultsToConfig (
    defaultConfig: Partial<GidConfigInterface>,
    initConfig: GidConfigInterface,
  ): GidConfigInterface {
    const conf: GidConfigInterface = { ...initConfig }
    Object.keys(defaultConfig).forEach((key: string) => {
      conf[key] = initConfig[key] === undefined || initConfig[key] === 'undefined'
        ? defaultConfig[key]
        : initConfig[key]
    })

    return conf
  }

  /**
   * sets up the popup widget
   */

  function getWindowRenderProps (): PopupWinProps {
    const urlParametersString: string = getGidcParametersString();
    const url: string | undefined =
      config.gid_connect_url !== undefined
        ? `${config.gid_connect_url}?${urlParametersString}`
        : `${gid_connect_url}?${urlParametersString}`;

    const winName: string =
      config.window_name !== undefined ? config.window_name : 'web-client';

    const customFeatures: object =
      config.window_features !== undefined ? config.window_features : {};
    const winFeatures: object = { ...windowFeatures, ...customFeatures };

    const winSerializedFeatures: string = serialize(winFeatures);

    return {
      url,
      winFeatures,
      winName,
      winSerializedFeatures,
    };
  }

  /**
   * Returns GIDC parameter string
   */
  function getGidcParametersString (): string {
    const state: string | undefined = config.default_oauth_options.state;

    const urlParameters: URLGidcParams = {
      acrc_id: config.acrc_uuid !== null ? config.acrc_uuid : undefined,
      client_id: config.client_id,
      nonce: config.nonce,
      redirect_uri: config.redirect_url,
      response_type: getOauthResponseTypeParam(config.default_oauth_options.grant_type),
      scope: config.scopes !== undefined ? config.scopes.join(',') : undefined,
      state: state !== undefined && state.length !== 0 ? encodeURIComponent(state) : undefined,
    };

    let urlParametersString: string = '';

    Object.keys(urlParameters).forEach((key: string) => {
      if (urlParameters[key] !== undefined) {
        urlParametersString += (`${key}=${urlParameters[key]}&`);
      }
    });

    return urlParametersString.substring(0, urlParametersString.length - 1);
  }

  /**
   * Get Oauth response type param from enum
   * Returning token by default since sdk is init-ted with refresh_token granttype which needs token to be sent here
   * as the auth code flow is not implemented in the sdk
   * @param grantType enum of possible grant types
   */
  function getOauthResponseTypeParam (grantType: GrantTypes): string {
    let oAuthResponseType: string;

    switch (grantType) {
      case GrantTypes.implicit:
        oAuthResponseType = 'token';
        break;
      case GrantTypes.code:
        oAuthResponseType = 'code';
        break;
      default:
        oAuthResponseType = 'token';
    }

    return oAuthResponseType;
  }

  /**
   * calls a popup window and requests to render the given flow(s)
   */
  function render (
    flowToRender?: FlowTypes | FlowTypes[],
    uuid: string[] = [],
  ): void {
    if (flowToRender !== undefined) {
      currentlyLoadedFlow = flowToRender;
    }

    if (uuid.length > 0) {
      app_uuid = uuid;
    }

    const winProps: PopupWinProps = getWindowRenderProps();
    popupName = winProps.winName;
    if (config.iframe_container_id !== undefined) {
      clearIFrameContainer();

      const iframeContainer: HTMLElement | null = document.getElementById(
        config.iframe_container_id,
      );
      if (iframeContainer === null) {
        throw new Error('IFrame container is missing.');
      }

      const close_div: HTMLDivElement = document.createElement('div');
      close_div.style.cssFloat = 'right';
      close_div.style.cursor = 'pointer';
      if (config.iframe_close_style !== undefined) {
        Object.assign(close_div.style, config.iframe_close_style);
      }
      close_div.innerText = 'X';
      close_div.onclick = (): boolean => {
        clearIFrameContainer();
        iframeContainer.style.pointerEvents = 'none';

        return false;
      };

      iframeContainer.appendChild(close_div);

      const frame: HTMLIFrameElement = document.createElement('iframe');
      frame.setAttribute('id', popupName);
      frame.style.width = '481px';
      frame.style.height = '641px';
      frame.style.backgroundColor = 'white';
      if (config.iframe_style !== undefined) {
        Object.assign(frame.style, config.iframe_style);
      }
      frame.src = winProps.url;

      iframeContainer.appendChild(frame);
      const iframeWindow: Window | null = frame.contentWindow;

      frame.onload = (): void => {
        transceiver.addTarget(popupName, iframeWindow);
      };
    } else {
      const popup: Window | null = window.open(
        winProps.url,
        winProps.winName,
        winProps.winSerializedFeatures,
      );
      transceiver.addTarget(popupName, popup);
    }
  }

  /**
   * Start the missing attestation flow
   */
  export function missingAttestations (): void {
    render(FlowTypes.MISSING_ATTESTATIONS);
  }

  /**
   * Start the upgrade to globaliD flow
   */
  export function upgradeToGlobaliD (): void {
    render(FlowTypes.UPGRADE_TO_GLOBALID);
  }

  /**
   * Start the attestation request approval flow
   */
  export function approveAttestationRequest (request_uuid: string): void {
    render(FlowTypes.ATTESTATION_APPROVAL, [request_uuid]);
  }

  /**
   * Renders the Signup flow with the given attestation
   */
  export function signup (attestationType: FlowTypes | FlowTypes[]): void {
    const flow: FlowTypes[] = !Array.isArray(attestationType)
      ? [attestationType]
      : attestationType;

    render([FlowTypes.SIGN_UP, ...flow]);
  }

  /**
   * Renders the login flow
   */
  export function login (): void {
    render(FlowTypes.LOGIN);
  }

  /**
   * Change password interface
   */
  export function changePassword (): void {
    render(FlowTypes.CHANGE_PASSWORD);
  }

  /**
   * Renders the given attestation flow
   */
  export function attest (uuid: string[]): void {
    render(FlowTypes.ATTESTATION, uuid);
  }

  /**
   * Renders the given sandbox attestation flow
   */
  export function attestSandbox (uuid: string[]): void {
    render(FlowTypes.SANDBOX_ATTESTATION, uuid);
  }

  /**
   * handles user completion response
   */
  export function onComplete (callback: Callback): void {
    completeListener = callback;
  }

  /**
   * handles error response
   */
  export function onError (callback: Callback): void {
    errorListener = callback;
  }

  /**
   * clears local data
   */
  export function clear (): void {
    popupName = undefined;
    app_uuid = [];
    currentlyLoadedFlow = [FlowTypes.SIGN_UP];
  }

  /**
   * Clears the IFrame container.
   */
  export function clearIFrameContainer (): void {
    if (config.iframe_container_id !== undefined) {
      const iframeContainer: HTMLElement | null = document.getElementById(
        config.iframe_container_id,
      );
      if (iframeContainer !== null) {
        // while loop is needed for clearing container element
        // tslint:disable-next-line:no-loop-statement strict-boolean-expressions
        while (iframeContainer.firstChild) {
          iframeContainer.removeChild(iframeContainer.firstChild);
        }
      }
    }
  }
}
