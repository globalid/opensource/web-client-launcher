/**
 * @file default popup window features
 */
// Fix for making popup centered on parent window
// SO link - https://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen

const popupWidth: number = 480;
const popupHeight: number = 640;

export const windowFeatures: {
  height: number;
  left: number;
  resizable: boolean;
  scrollbars: boolean;
  top: number;
  width: number;
} = {
  height: popupHeight,
  left: window.screen.availWidth / 2 - (popupWidth / 2),
  resizable: false,
  scrollbars: false,
  top: window.screen.availHeight / 2 - (popupHeight / 2),
  width: popupWidth,
};

try {
  windowFeatures.left = window.top.outerWidth / 2 + window.top.screenX - (popupWidth / 2);
  windowFeatures.top = window.top.outerHeight / 2 + window.top.screenY - (popupHeight / 2);
} catch { } // tslint:disable-line: no-empty
