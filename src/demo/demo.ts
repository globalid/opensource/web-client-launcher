/**
 * @file demo of gid public interface
 */
import { Callback, CallbackPayload, ConfigInterface, FlowTypes, gid, GrantTypes } from '../';
// tslint:disable-next-line:no-backbone-get-set-outside-model
const use_iframe: boolean = new URL(window.location.href).searchParams.get('use_iframe') === 'true';
// tslint:disable-next-line:prefer-type-cast
const iframeLink: HTMLAnchorElement = document.getElementById('iframe_link') as HTMLAnchorElement;
// @ts-ignore
if (use_iframe) {
  iframeLink.innerText = 'Use Window Popup'
  iframeLink.href = `${window.location.pathname}`
} else {
  iframeLink.innerText = 'Use IFRAME Popup'
  iframeLink.href = `${window.location.pathname}?use_iframe=true`
}
// @ts-ignore
const client_id: string = document.querySelector(`meta[name='client_id']`)
  .getAttribute('content');

// @ts-ignore
const api_base_url: string = document.querySelector(`meta[name='api_base_url']`)
  .getAttribute('content');

// @ts-ignore
const agencies_url: string = document.querySelector(`meta[name='agencies_url']`)
  .getAttribute('content');

// @ts-ignore
const gid_connect_base_url: string = document.querySelector(`meta[name='gid_connect_base_url']`)
  .getAttribute('content');

// @ts-ignore
const redirect_url: string = document.querySelector(`meta[name='redirect_url']`)
  .getAttribute('content');

// @ts-ignore
const version: string = document.querySelector(`meta[name='version']`)
  .getAttribute('content');

let acrc_uuid: string | null = new URL(window.location.href).searchParams.get('acrc_id');

if (acrc_uuid === null) {
  // @ts-ignore
  acrc_uuid = document.querySelector(`meta[name='acrc_uuid']`)
    .getAttribute('content');
}

const config: ConfigInterface = {
  acrc_uuid: acrc_uuid,
  agencies_url,
  api_base_url,
  client_id,
  default_oauth_options: {
    grant_type: GrantTypes.code,
    state: 'this is a demo state',
  },
  gid_connect_base_url,
  redirect_url,
  scopes: ['public'],
  version,
};

const config_no_acrc: ConfigInterface = {
  agencies_url,
  api_base_url,
  client_id,
  default_oauth_options: {
    grant_type: GrantTypes.code,
    state: 'this is a demo state',
  },
  gid_connect_base_url,
  redirect_url,
  scopes: ['public'],
  version,
};

const onComplete: Callback = (payload: CallbackPayload): void => {
  // tslint:disable-next-line
  console.log(payload);
}

const onError: Callback = (payload: CallbackPayload): void => {
  // tslint:disable-next-line
  console.log(payload);
}

/**
 * attestation event handler
 * @param id id param for attest
 */
function attest (id: string): Function {
  return (): boolean => {
    gid.init(prepareConfig(config_no_acrc));
    gid.attest([id]); // Twilio app uuid
    gid.onComplete(onComplete);
    gid.onError(onError);

    return false;
  }
}

/**
 * Function for config moderation
 */
function prepareConfig (conf: ConfigInterface): ConfigInterface {
  const iframeContainer: HTMLElement | null = document.getElementById('iframe-container');
  if (iframeContainer !== null) {
    iframeContainer.style.pointerEvents = 'all';
    if (use_iframe) {
      Object.assign(conf, {
        iframe_close_style: {
          color: 'rgb(83, 190, 151)',
          'font-weight': 'bold',
        },
        iframe_container_id: 'iframe-container',
        iframe_style: {
          border: '2px solid rgb(83, 190, 151)',
          'border-radius': '4px',
          'box-shadow': '0 0 15px whitesmoke',
        },
      })
    }
  }

  return conf
}
// @ts-ignore
document.getElementById('button-sign-up').onclick = (): boolean => {
  gid.init(prepareConfig(config_no_acrc));
  gid.signup(FlowTypes.TWILIO);
  gid.onComplete(onComplete);
  gid.onError(onError);

  return false;
};

// @ts-ignore
document.getElementById('button-login').onclick = (): boolean => {
  gid.init(prepareConfig(config_no_acrc));
  gid.login();
  gid.onComplete(onComplete);
  gid.onError(onError);

  return false;
};

// @ts-ignore
document.getElementById('button-attest').onclick = attest('ea215db9-ce79-4bb3-b9eb-3d90f5455d52');

// @ts-ignore
document.getElementById('button-attest-mandrill').onclick = attest('d0086f64-7c38-44ce-99eb-9c4d70d38cd4');

// @ts-ignore
document.getElementById('button-attest-onfido').onclick = attest('857968ce-204c-4ccf-a216-d8df3f155339');

// @ts-ignore
document.getElementById('button-attest-au10tix').onclick = attest('e5723ee9-bb00-4f58-8494-3d11120d21be');

// @ts-ignore
document.getElementById('button-pw-change').onclick = (): boolean => {
  gid.init(prepareConfig(config_no_acrc));
  gid.changePassword();
  gid.onComplete(onComplete);
  gid.onError(onError);

  return false;
};

// @ts-ignore
document.getElementById('button-maf').onclick = (): boolean => {
  gid.init(prepareConfig(config));
  gid.missingAttestations();
  gid.onComplete(onComplete);
  gid.onError(onError);

  return false;
}
