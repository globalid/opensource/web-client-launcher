require('dotenv').config();
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: path.resolve('./src/demo'),

  devtool: 'source-map',

  entry: {
    app: './index.ts',
    demo: './demo.ts',
  },

  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: ['node_modules'],
        test: /\.ts?$/,
        use: ['awesome-typescript-loader', 'source-map-loader'],
      },
    ],
  },

  mode: 'production',

  resolve: {
    extensions: ['.ts', '.js'],
  },

  plugins: [
    new HtmlWebpackPlugin({
      chunks: ['demo'],
      filename: 'demo.html',
      meta: {
        client_id: process.env.CLIENT_ID,
        agencies_url: process.env.AGENCIES_URL,
        api_base_url: process.env.API_BASE_URL,
        gid_connect_base_url: process.env.GID_CONNECT_BASE_URL,
        redirect_url: process.env.REDIRECT_URL,
        acrc_uuid: process.env.ACRC_UUID,
        version: process.env.VERSION,
      },
      template: 'demo.html',
    }),
    new webpack.EnvironmentPlugin({
      API_BASE_URL: process.env.API_BASE_URL,
    }),
    new webpack.EnvironmentPlugin({
      AGENCIES_URL: process.env.AGENCIES_URL,
    }),
    new webpack.EnvironmentPlugin({
      VERSION: process.env.VERSION,
    }),
    new webpack.EnvironmentPlugin({
      GID_CONNECT_SIGNUP_URL: process.env.GID_CONNECT_SIGNUP_URL,
    })
  ],

  devServer: {
    contentBase: './src/demo',
    port: 3003,
  }
};
