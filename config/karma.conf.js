const webpack = require('webpack');

module.exports = function(config) {

  let files = [{ pattern: '../tests/spec-unit-bundle.js', watched: false }];
  let preprocessors = { '../tests/spec-unit-bundle.js': ['webpack', 'sourcemap'] };
  let coverageDir = './tests/coverage';
  let envPlugin;

  config.set({

    basePath: '',

    browserNoActivityTimeout: 50000,
    browsers: ['ChromeHeadlessNoSandbox'],
    concurrency: Infinity,

    coverageIstanbulReporter: {
      dir: coverageDir,
      fixWebpackSourcePaths: true,
      query: {
        esModules: true,
      },
      reports: ['html', 'lcov', 'text-summary'],
      verbose: true,
    },

    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },

    files,

    frameworks: ['jasmine'],

    port: 9876,

    preprocessors,

    reporters: ['spec', 'coverage-istanbul'],

    singleRun: true,

    webpack: {
      devtool: 'inline-source-map',
      mode: 'development',
      module: {
        rules: [
          {
            enforce: 'pre',
            exclude: ['node_modules'],
            test: /\.ts?$/,
            use: ['awesome-typescript-loader', 'source-map-loader'],
          },
          {
            enforce: 'post',
            exclude: /(node_modules|\.unit\.ts$)/,  // excludes jasmine `*.unit.ts` from coverage
            loader: 'istanbul-instrumenter-loader',
            options: {
              esModules: true,
            },
            test: /src\/.+\.ts$/,
          },
        ],
      },
      output: {
        devtoolModuleFilenameTemplate: ':[resource-path]',
      },
      resolve: {
        extensions: ['.ts', '.js'],
      },

      plugins: (envPlugin) ? [envPlugin] : undefined,
    },

    webpackMiddleware: {
      noInfo: true,
      stats: 'errors-only',
    },
  });
};
