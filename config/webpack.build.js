require('dotenv').config();
const webpack = require('webpack');
const path = require('path');
const  CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  context: path.resolve('./src/'),

  devtool: 'source-map',

  entry: './index.ts',

  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: ['node_modules'],
        test: /\.ts?$/,
        use: ['awesome-typescript-loader', 'source-map-loader'],
      },
    ],
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, '../dist'),
    libraryTarget: 'umd',
  },

  mode: 'production',

  resolve: {
    extensions: ['.ts', '.js'],
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      GID_CONNECT_SIGNUP_URL: process.env.GID_CONNECT_SIGNUP_URL,
    }),
    new CopyWebpackPlugin([
      { from: 'demo/images', to: 'demo/images' }
    ]),
  ],

  devServer: {
    contentBase: './src/demo',
    port: 3000,
  }
};
