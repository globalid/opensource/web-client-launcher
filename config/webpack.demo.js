require('dotenv').config();
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  context: path.resolve('./src/demo'),

  devtool: 'source-map',

  entry: './demo.ts',

  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: ['node_modules'],
        test: /\.ts?$/,
        use: ['awesome-typescript-loader', 'source-map-loader'],
      },
    ],
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, '../dist/demo'),
    libraryTarget: 'umd',
  },

  mode: 'production',

  resolve: {
    extensions: ['.ts', '.js'],
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'demo.html',
      meta: {
        client_id: process.env.CLIENT_ID,
        agencies_url: process.env.AGENCIES_URL,
        api_base_url: process.env.API_BASE_URL,
        gid_connect_base_url: process.env.GID_CONNECT_BASE_URL,
        redirect_url: process.env.REDIRECT_URL,
        acrc_uuid: process.env.ACRC_UUID,
        version: process.env.VERSION,
      },
    }),
    new webpack.EnvironmentPlugin({
      GID_CONNECT_SIGNUP_URL: process.env.GID_CONNECT_SIGNUP_URL,
    })
  ],

  devServer: {
    contentBase: './src/demo',
    port: 3000,
  }
};
